CC=gcc

DIR_SRC=srcs
DIR_OBJ=objs

SRCS=$(shell find $(DIR_SRC) -type f)

INCLUDES=-Iinclude -Ilibft/includes

CFLAGS=-Wall -Werror -Wextra $(INCLUDES)
LDFLAGS=-Llibft -lft

NAME=push_swap

OBJS=$(patsubst $(DIR_SRC)/%,$(DIR_OBJ)/%,$(SRCS:.c=.o))

all: $(NAME)

re : fclean all

clean: libft_clean
	rm -rf $(DIR_OBJ)

fclean: libft_fclean clean
	rm -rf ${NAME}

$(NAME): $(OBJS)
	make -C libft
	$(CC) -o $@ $^ $(LDFLAGS)

$(DIR_OBJ)/%.o : $(DIR_SRC)/%.c
	mkdir -p $(dir $@)
	$(CC) -o $@ -c $< $(CFLAGS)

libft_clean:
	make clean -C libft

libft_fclean:
	make fclean -C libft

run: all
	./push_swap `ruby -e "puts (-5000..5000).to_a.reverse.insert(rand(8000) + 1000, 10001).join(' ')"` | wc -l
	./push_swap `ruby -e "puts (-1000..1000).to_a.shuffle.join(' ')"` | wc -l
	./push_swap 1 2 3 6 5 | wc -l
	./push_swap 3 2 1 | wc -l
	./push_swap 2 1 3 5 6 | wc -l

.PHONY: clean fclean re all $(NAME)
