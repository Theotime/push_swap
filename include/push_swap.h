/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/17 21:21:10 by triviere          #+#    #+#             */
/*   Updated: 2015/12/17 21:24:40 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include <libft.h>
# include <stdio.h>
# include <stdlib.h>
# include <stddef.h>

typedef struct s_lst	t_lst;
typedef struct s_el		t_el;

struct					s_lst {
	t_el				*start;
	t_el				*end;
	char				*name;
	int					len;
	int					max;
	int					min;
};

struct					s_el {
	struct s_el			*next;
	struct s_el			*prev;
	int					val;
};

void					display_lst(t_lst lst);
void					play(t_lst *lst_a, t_lst *lst_b);

void					s(t_lst *lst);
void					ss(t_lst *lst_a, t_lst *lst_b);
void					p(t_lst *lst1, t_lst *lst2);
void					r_left(t_lst *lst);
void					r_right(t_lst *lst);

void					cmd_pa(t_lst *lst_a, t_lst *lst_b);
void					cmd_pb(t_lst *lst_a, t_lst *lst_b);
void					cmd_pb_with_sort(t_lst *lst_a, t_lst *lst_b);
void					cmd_sa(t_lst *lst_a);
void					cmd_sb(t_lst *lst_b);
void					cmd_ss(t_lst *lst_a, t_lst *lst_b);
void					cmd_ra(t_lst *lst_a);
void					cmd_rb(t_lst *lst_b);
void					cmd_rr(t_lst *lst_a, t_lst *lst_b);
void					cmd_rra(t_lst *lst_a);
void					cmd_rrb(t_lst *lst_b);
void					cmd_rrr(t_lst *lst_a, t_lst *lst_b);

void					push_back(t_lst *lst, t_el *el);
void					push_front(t_lst *lst, t_el *el);
void					lst_min_max(t_lst *lst);
t_el					*last_elem(t_lst *lst);
t_el					*first_elem(t_lst *lst);
t_lst					*init_lst(char *name);
t_el					*init_el(int val);
int						has_minus(t_lst *lst, int n);
int						has_more(t_lst *lst, int n);
t_lst					*clone_lst(t_lst *src, char *name);
t_el					*clone_el(t_el *el);
void					order_lst(t_lst *b, int n);
void					next_move_part_one(t_lst *a);
void					next_move_part_two(t_lst *a, t_lst *b);

void					resolve(t_lst *lst_a, t_lst *lst_b);

int						is_finish(t_lst *lst_a, t_lst *lst_b);
int						is_sort(t_lst *lst);
int						is_sort_asc(t_lst *lst);

#endif
