/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <triviere@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/24 04:08:09 by triviere          #+#    #+#             */
/*   Updated: 2015/12/21 17:51:44 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

int		ft_atoi(char const *nbr)
{
	int		i;
	int		result;
	int		neg;

	if ((char)*nbr == '\0')
		return (0);
	result = 0;
	i = 0;
	neg = 0;
	while (nbr[i] == ' ' || (nbr[i] >= 9 && nbr[i] <= 13))
		i++;
	if (nbr[i] == '-')
		neg = 1;
	if (nbr[i] == '-' || nbr[i] == '+')
		i = 1;
	while (nbr[i] >= '0' && nbr[i] <= '9')
	{
		if (result > 214748364 || (result == 214748364
			&& ((!neg && (nbr[i] - 48) > 7) || (neg && (nbr[i] - 48) > 8))))
			return (0);
		result *= 10;
		result += nbr[i++] - 48;
	}
	return (neg ? result * -1 : result);
}
