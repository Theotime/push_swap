/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <triviere@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/30 08:11:16 by triviere          #+#    #+#             */
/*   Updated: 2013/11/30 08:44:15 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*new;
	t_list	tmp;

	new = ft_lstnew(NULL, 0);
	if (new)
	{
		while (lst->next)
		{
			tmp = *lst;
			ft_lstadd(&new, f(&tmp));
			lst = lst->next;
		}
		return (new);
	}
	return (NULL);
}
