/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <triviere@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 04:05:53 by triviere          #+#    #+#             */
/*   Updated: 2013/11/22 06:09:35 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void	*ft_memmove(void *s1, void const *s2, size_t n)
{
	char	*tmp;

	tmp = ft_strdup(s2);
	ft_memcpy(s1, tmp, n);
	free(tmp);
	return (s1);
}
