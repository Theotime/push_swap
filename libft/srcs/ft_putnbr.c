/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <triviere@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/19 13:56:27 by triviere          #+#    #+#             */
/*   Updated: 2013/11/19 13:56:30 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

static	void	ft_putnbr_rec(unsigned int nbr)
{
	char	c;

	if (!nbr)
		return ;
	c = nbr % 10 + 48;
	ft_putnbr_rec(nbr / 10);
	ft_putchar(c);
}

void			ft_putnbr(int nbr)
{
	if (nbr < 0)
	{
		ft_putchar('-');
		ft_putnbr_rec(nbr * -1);
	}
	else if (nbr)
		ft_putnbr_rec(nbr);
	else
		ft_putchar('0');
}
