/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <triviere@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 11:16:40 by triviere          #+#    #+#             */
/*   Updated: 2013/12/04 04:43:07 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strdup(char const *s)
{
	char	*tmp;
	int		i;

	i = 0;
	tmp = ft_strnew(ft_strlen((char *)s));
	if (tmp != NULL)
	{
		while (((char *)s)[i])
		{
			tmp[i] = ((char *)s)[i];
			i++;
		}
		tmp[i] = '\0';
	}
	return (tmp);
}
