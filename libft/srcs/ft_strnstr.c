/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <triviere@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/25 15:44:37 by triviere          #+#    #+#             */
/*   Updated: 2013/11/30 08:54:51 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strnstr(char const *s1, char const *s2, size_t len)
{
	int			i;
	size_t		n;

	if (!*s2)
		return ((char *)s1);
	n = 0;
	i = 0;
	while (*s1 && n <= len)
	{
		if (((char *)s2)[i] == '\0')
			return ((char *)s1 - i);
		else if (*s1 == s2[i])
			i++;
		else if (i > 0)
		{
			s1 -= i;
			n -= i;
			i = 0;
		}
		s1++;
		n++;
	}
	return ((*s1 == s2[i] && !s2[i] && n <= len) ? (char *)(s1 - i) : 0);
}
