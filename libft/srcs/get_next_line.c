/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <triviere@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/02 03:07:11 by triviere          #+#    #+#             */
/*   Updated: 2015/12/15 10:36:36 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/get_next_line.h"

static	t_fd	*get_file_descriptor(int const fd)
{
	static	t_fd	*sys = 0;
	t_fd			*cursor;

	if (sys)
	{
		cursor = sys;
		while (cursor)
		{
			if (cursor->fd == fd)
				return (cursor);
			cursor = cursor->next;
		}
	}
	cursor = malloc(sizeof(t_fd));
	cursor->fd = fd;
	cursor->buff = NULL;
	cursor->ret = 1;
	cursor->next = sys;
	sys = cursor;
	return (cursor);
}

static	void	get_buff(t_fd *sys)
{
	char	*remain;
	char	*tmp;

	remain = ft_strnew(BUFF_SIZE);
	while (!sys->buff || (!ft_strchr(sys->buff, '\n') && sys->ret > 0))
	{
		tmp = sys->buff;
		sys->ret = read(sys->fd, remain, BUFF_SIZE);
		remain[sys->ret] = 0;
		sys->buff = ft_strjoin(tmp, remain);
		free(tmp);
	}
	free(remain);
}

static	void	get_line(t_fd *sys, char **line)
{
	char	*tmp;
	int		i;

	i = 0;
	get_buff(sys);
	tmp = sys->buff;
	if (tmp)
	{
		while (tmp[i] && tmp[i] && tmp[i] != '\n')
		{
			i++;
		}
		*line = ft_strsub(tmp, 0, i);
		if (tmp[i] == '\n')
			i++;
		sys->buff = ft_strsub(tmp, i, ft_strlen(sys->buff) - i);
	}
	free(tmp);
}

int				get_next_line(int const fd, char **line)
{
	t_fd	*sys;

	sys = get_file_descriptor(fd);
	get_line(sys, line);
	return (sys->ret <= 0 && !ft_strlen(*line) ? sys->ret : 1);
}
