/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   actions.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/17 19:15:54 by triviere          #+#    #+#             */
/*   Updated: 2015/12/17 21:05:17 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

void		s(t_lst *lst)
{
	t_el		*tmp;

	if (lst->len < 2)
		return ;
	tmp = lst->end;
	lst->end = tmp->prev;
	if (lst->len > 2)
		lst->end->prev->next = tmp;
	else if (lst->len == 2)
		lst->start = tmp;
	tmp->prev = lst->end->prev;
	tmp->next = lst->end;
	lst->end->next = NULL;
	lst->end->prev = tmp;
}

void		p(t_lst *lst1, t_lst *lst2)
{
	t_el		*tmp;

	if (lst1->len < 1)
		return ;
	tmp = last_elem(lst1);
	push_back(lst2, tmp);
}

void		r_left(t_lst *lst)
{
	t_el		*tmp;

	if (lst->len < 2)
		return ;
	tmp = lst->start;
	lst->start = tmp->next;
	lst->start->prev = NULL;
	tmp->next = NULL;
	tmp->prev = lst->end;
	lst->end->next = tmp;
	lst->end = tmp;
}

void		r_right(t_lst *lst)
{
	t_el		*tmp;

	if (lst->len < 2)
		return ;
	tmp = lst->end;
	tmp->prev->next = NULL;
	lst->end = tmp->prev;
	tmp->next = lst->start;
	tmp->prev = NULL;
	tmp->next->prev = tmp;
	lst->start = tmp;
}
