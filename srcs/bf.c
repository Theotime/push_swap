/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bf.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/17 19:18:54 by triviere          #+#    #+#             */
/*   Updated: 2015/12/17 19:24:27 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

void		next_move_part_one(t_lst *a)
{
	if (a->end->val > a->end->prev->val)
		cmd_sa(a);
	else if (a->start->next->val > a->start->val)
	{
		cmd_rra(a);
		cmd_rra(a);
		cmd_sa(a);
	}
	else
		cmd_rra(a);
}

void		next_move_part_two(t_lst *a, t_lst *b)
{
	if (a->len > 1 && a->end->val > a->end->prev->val)
		cmd_sa(a);
	else if (a->len > 1 && a->end->val > a->start->val \
			&& a->end->val != a->max)
	{
		cmd_rra(a);
		cmd_sa(a);
	}
	else
		cmd_pb_with_sort(a, b);
}
