/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   command_push.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/17 19:16:06 by triviere          #+#    #+#             */
/*   Updated: 2015/12/17 21:29:11 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

void		cmd_pa(t_lst *lst_a, t_lst *lst_b)
{
	p(lst_b, lst_a);
	ft_putstr("PA ");
}

void		order_lst(t_lst *b, int n)
{
	while (b->end->val > n || b->start->val < n)
	{
		if (b->end->val == b->max && (n > b->max || n < b->min))
			break ;
		if (b->end->val > n && b->end->prev->val < n)
			cmd_rb(b);
		else
			cmd_rrb(b);
	}
}

void		cmd_pb(t_lst *lst_a, t_lst *lst_b)
{
	p(lst_a, lst_b);
	ft_putstr("PB ");
}

void		cmd_pb_with_sort(t_lst *lst_a, t_lst *lst_b)
{
	if (lst_b->len > 1 && lst_a->len > 0)
		order_lst(lst_b, lst_a->end->val);
	p(lst_a, lst_b);
	ft_putstr("PB ");
}
