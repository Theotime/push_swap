/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   command_rotate_left.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/17 19:16:10 by triviere          #+#    #+#             */
/*   Updated: 2015/12/17 21:29:24 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

void			cmd_rra(t_lst *lst_a)
{
	r_left(lst_a);
	ft_putstr("RRA ");
}

void			cmd_rrb(t_lst *lst_b)
{
	r_left(lst_b);
	ft_putstr("RRB ");
}

void			cmd_rrr(t_lst *lst_a, t_lst *lst_b)
{
	r_left(lst_a);
	r_left(lst_b);
	ft_putstr("RRC ");
}
