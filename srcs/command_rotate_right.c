/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   command_rotate_right.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/17 19:16:14 by triviere          #+#    #+#             */
/*   Updated: 2015/12/17 21:29:35 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

void		cmd_ra(t_lst *lst_a)
{
	r_right(lst_a);
	ft_putstr("RA ");
}

void		cmd_rb(t_lst *lst_b)
{
	r_right(lst_b);
	ft_putstr("RB ");
}

void		cmd_rr(t_lst *lst_a, t_lst *lst_b)
{
	r_right(lst_a);
	r_right(lst_b);
	ft_putstr("RR ");
}
