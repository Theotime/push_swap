/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   command_swipe.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/17 19:16:20 by triviere          #+#    #+#             */
/*   Updated: 2015/12/17 21:30:37 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

void		cmd_sa(t_lst *lst_a)
{
	s(lst_a);
	ft_putstr("SA ");
}

void		cmd_sb(t_lst *lst_b)
{
	s(lst_b);
	ft_putstr("SB ");
}

void		cmd_ss(t_lst *lst_a, t_lst *lst_b)
{
	s(lst_a);
	s(lst_b);
	ft_putstr("SS ");
}
