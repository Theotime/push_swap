/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dev_function.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/17 19:16:24 by triviere          #+#    #+#             */
/*   Updated: 2015/12/17 21:30:25 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

void	display_lst(t_lst lst)
{
	t_el	*cur;

	cur = lst.start;
	ft_putstr("[");
	ft_putstr(lst.name);
	ft_putstr("] -> ");
	while (cur)
	{
		ft_putnbr(cur->val);
		ft_putstr(" ");
		cur = cur->next;
	}
	ft_putstr("\n");
}

int		exec_cmd(t_lst *lst_a, t_lst *lst_b, char *cmd)
{
	if (!ft_strcmp(cmd, "quit\n") || !ft_strcmp(cmd, "q\n"))
		return (0);
	else if (!ft_strcmp(cmd, "sa\n"))
		cmd_sa(lst_a);
	else if (!ft_strcmp(cmd, "sb\n"))
		cmd_sb(lst_b);
	else if (!ft_strcmp(cmd, "ss\n"))
		cmd_ss(lst_a, lst_b);
	else if (!ft_strcmp(cmd, "pa\n"))
		cmd_pa(lst_a, lst_b);
	else if (!ft_strcmp(cmd, "pb\n"))
		cmd_pb(lst_a, lst_b);
	else if (!ft_strcmp(cmd, "ra\n"))
		cmd_ra(lst_a);
	else if (!ft_strcmp(cmd, "rb\n"))
		cmd_rb(lst_b);
	else if (!ft_strcmp(cmd, "rr\n"))
		cmd_rr(lst_a, lst_b);
	else if (!ft_strcmp(cmd, "rra\n"))
		cmd_rra(lst_a);
	else if (!ft_strcmp(cmd, "rrb\n"))
		cmd_rrb(lst_b);
	else if (!ft_strcmp(cmd, "rrr\n"))
		cmd_rrr(lst_a, lst_b);
	return (1);
}

void	play(t_lst *lst_a, t_lst *lst_b)
{
	char		cmd[200];

	display_lst(*lst_a);
	display_lst(*lst_b);
	while (is_finish(lst_a, lst_b) == 0)
	{
		ft_putstr("$-> ");
		fgets(cmd, sizeof(cmd), stdin);
		if (!exec_cmd(lst_a, lst_b, cmd))
			break ;
		display_lst(*lst_a);
		display_lst(*lst_b);
	}
}
