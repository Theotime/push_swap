/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   el.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/17 19:16:27 by triviere          #+#    #+#             */
/*   Updated: 2015/12/17 21:08:00 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

t_el		*init_el(int val)
{
	t_el		*el;

	el = malloc(sizeof(t_el));
	el->next = NULL;
	el->prev = NULL;
	el->val = val;
	return (el);
}

t_el		*clone_el(t_el *el)
{
	t_el	*clone;

	clone = init_el(el->val);
	return (clone);
}
