/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   logic.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/17 19:16:37 by triviere          #+#    #+#             */
/*   Updated: 2015/12/17 21:10:33 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		is_sort(t_lst *lst)
{
	t_el		*cur;
	t_el		*next;
	int			order;

	cur = lst->start;
	order = cur && lst->len > 1 ? cur->val > cur->next->val : 0;
	while (cur)
	{
		next = cur->next ? cur->next : lst->start;
		if ((!order && cur->val == lst->max) \
			|| (order && next->val == lst->max))
			order = !order;
		if (order && cur->val < next->val)
			return (0);
		else if (!order && cur->val > next->val)
			return (0);
		cur = cur->next;
	}
	return (1);
}

int		is_sort_asc(t_lst *lst)
{
	t_el		*cur;
	int			old;

	cur = lst->start;
	old = cur->val;
	while (cur)
	{
		if (cur->val > old)
			return (0);
		old = cur->val;
		cur = cur->next;
	}
	return (1);
}

int		is_finish(t_lst *lst_a, t_lst *lst_b)
{
	int			tmp;
	t_el		*cur;

	if (lst_b->len > 0)
		return (0);
	cur = lst_a->start;
	tmp = cur->val;
	while (cur)
	{
		if (cur->val > tmp)
			return (0);
		tmp = cur->val;
		cur = cur->next;
	}
	return (1);
}
