/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/17 19:16:43 by triviere          #+#    #+#             */
/*   Updated: 2015/12/17 21:01:34 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

void		push_back(t_lst *lst, t_el *el)
{
	if (lst->end == NULL)
	{
		lst->max = el->val;
		lst->min = el->val;
		lst->start = el;
	}
	else
	{
		lst->end->next = el;
		el->prev = lst->end;
	}
	if (el->val < lst->min)
		lst->min = el->val;
	if (el->val > lst->max)
		lst->max = el->val;
	lst->end = el;
	++lst->len;
}

void		push_front(t_lst *lst, t_el *el)
{
	if (lst->start == NULL)
	{
		lst->max = el->val;
		lst->min = el->val;
		lst->end = el;
	}
	else
	{
		lst->start->prev = el;
		el->next = lst->start;
		lst->start = el;
	}
	if (el->val < lst->min)
		lst->min = el->val;
	if (el->val > lst->max)
		lst->max = el->val;
	lst->start = el;
	++lst->len;
}

t_el		*last_elem(t_lst *lst)
{
	t_el		*el;

	el = lst->end;
	if (lst->len > 1)
	{
		lst->end = el->prev;
		lst->end->next = NULL;
	}
	else
	{
		lst->end = NULL;
		lst->start = NULL;
	}
	el->prev = NULL;
	el->next = NULL;
	--lst->len;
	lst_min_max(lst);
	return (el);
}

t_el		*first_elem(t_lst *lst)
{
	t_el	*el;

	el = lst->start;
	if (lst->len > 1)
	{
		lst->start = el->next;
		lst->start->prev = NULL;
	}
	else
	{
		lst->start = NULL;
		lst->end = NULL;
	}
	el->prev = NULL;
	el->next = NULL;
	--lst->len;
	lst_min_max(lst);
	return (el);
}

t_lst		*init_lst(char *name)
{
	t_lst		*lst;

	lst = malloc(sizeof(t_lst));
	lst->name = name;
	lst->len = 0;
	lst->start = NULL;
	lst->end = NULL;
	lst->min = 0;
	lst->max = 0;
	return (lst);
}
