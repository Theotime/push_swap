/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_additional.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/17 19:15:41 by triviere          #+#    #+#             */
/*   Updated: 2015/12/17 21:09:15 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	lst_min_max(t_lst *lst)
{
	t_el	*cur;
	int		min;
	int		max;

	if (lst->len < 1)
	{
		lst->min = 0;
		lst->max = 0;
		return ;
	}
	cur = lst->start;
	min = lst->start->val;
	max = min;
	while (cur)
	{
		if (min > cur->val)
			min = cur->val;
		if (max < cur->val)
			max = cur->val;
		cur = cur->next;
	}
	lst->max = max;
	lst->min = min;
}

int		has_minus(t_lst *lst, int n)
{
	t_el	*cur;

	cur = lst->start;
	while (cur)
	{
		if (cur->val < n)
			return (1);
		cur = cur->next;
	}
	return (0);
}

int		has_more(t_lst *lst, int n)
{
	t_el	*cur;

	cur = lst->start;
	while (cur)
	{
		if (cur->val > n)
			return (1);
		cur = cur->next;
	}
	return (0);
}

t_lst	*clone_lst(t_lst *src, char *name)
{
	t_el		*cur;
	t_lst		*dest;

	cur = src->start;
	dest = init_lst(name);
	while (cur)
	{
		push_back(dest, clone_el(cur));
		cur = cur->next;
	}
	return (dest);
}
