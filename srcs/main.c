/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/17 19:15:32 by triviere          #+#    #+#             */
/*   Updated: 2015/12/21 17:48:09 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

int		is_zero(char *str)
{
	int		i;

	i = str[0] == '+' || str[0] == '-' ? 1 : 0;
	while (str[i])
	{
		if (str[i] != '0')
			return (0);
		++i;
	}
	return (1);
}

int		is_exist(t_lst *lst, int n)
{
	t_el	*cur;

	cur = lst->start;
	while (cur)
	{
		if (cur->val == n)
			return (1);
		cur = cur->next;
	}
	return (0);
}

int		init_lsts(t_lst *lst_a, char **elems, int len)
{
	int		i;
	int		n;
	t_el	*el;

	i = 0;
	while (i < len)
	{
		if (!ft_isnumeric(elems[i]))
			return (0);
		n = ft_atoi(elems[i]);
		if ((n == 0 && !is_zero(elems[i])) || is_exist(lst_a, n))
			return (0);
		el = init_el(ft_atoi(elems[i]));
		push_front(lst_a, el);
		++i;
	}
	return (1);
}

int		main(int ac, char **av)
{
	t_lst		*lst_a;
	t_lst		*lst_b;
	int			interactive;

	if (ac < 2)
		return (0);
	interactive = (ft_strcmp(av[1], "-i") == 0);
	if (interactive && ac < 3)
		return (0);
	lst_a = init_lst("A");
	lst_b = init_lst("B");
	if (init_lsts(lst_a, av + 1 + interactive, ac - (1 + interactive)))
	{
		if (interactive)
			play(lst_a, lst_b);
		else
		{
			resolve(lst_a, lst_b);
			ft_putchar('\n');
		}
	}
	else
		ft_putstr("ERROR\n");
	return (0);
}
