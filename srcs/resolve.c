/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   resolve.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/17 19:15:24 by triviere          #+#    #+#             */
/*   Updated: 2015/12/17 19:24:48 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

void		cmd_max_b(t_lst *b, int val)
{
	t_el		*cur;
	int			n;
	int			pos;

	cur = b->start;
	n = 0;
	pos = 0;
	while (cur)
	{
		if (!pos && cur->val == val)
		{
			pos = (n > (b->len / 2)) + 1;
			break ;
		}
		cur = cur->next;
		++n;
	}
	while (n >= 0)
	{
		if (pos == 1)
			cmd_rrb(b);
		else
			cmd_rb(b);
		--n;
	}
}

int			end_is_order(t_lst *lst)
{
	t_el	*cur;
	int		n;

	cur = lst->end;
	n = 1;
	while (cur->prev)
	{
		if (cur->val > cur->prev->val)
			break ;
		cur = cur->prev;
		++n;
	}
	return (n > lst->len / 2 || lst->start->val == lst->max);
}

int			start_is_order(t_lst *lst)
{
	t_el		*cur;
	int			n;

	n = 0;
	cur = lst->start;
	while (cur->next)
	{
		if (cur->val < cur->next->val)
			break ;
		cur = cur->next;
		++n;
	}
	return (n > lst->len / 2 && lst->end->val != lst->max);
}

void		next_move(t_lst *a, t_lst *b)
{
	if (a->len > 1 && !is_sort(a) && (end_is_order(a) || start_is_order(a)))
		next_move_part_one(a);
	else if ((a->len && !is_sort(a)) || (a->len && a->max < b->max))
		next_move_part_two(a, b);
	else if (!a->len && b->end->val < b->max)
		cmd_max_b(b, b->max);
	else if (b->len && b->end->val == b->max)
	{
		if (a->start->val > b->end->val && a->start->val != a->max)
			cmd_rra(a);
		else
			cmd_pa(a, b);
	}
	else if (b->len > 1)
		cmd_rrb(b);
	else
		cmd_ra(a);
}

void		resolve(t_lst *a, t_lst *b)
{
	while (is_finish(a, b) == 0)
		next_move(a, b);
}
